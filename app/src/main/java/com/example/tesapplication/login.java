package com.example.tesapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.preference.PreferenceManager;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;

import org.json.JSONObject;

import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import API.ApiClient;
import API.PasienRequest;
import API.PasienResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class login extends Fragment {
    private Button button, toRegister;
    private EditText NO_PENGENAL, passLogin;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login, container, false);
        NO_PENGENAL = v.findViewById(R.id.nik);
        passLogin = v.findViewById(R.id.passLogin);
        button = (Button) v.findViewById(R.id.loginBtn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button.setEnabled(false);
                button.setText("Mengecek data ...");
                String nik = NO_PENGENAL.getText().toString();
                String pass = passLogin.getText().toString();
                pasienLogin(createRequest(nik, MD5(pass)));
//                openDashboard();
            }
        });
        toRegister = v.findViewById(R.id.toRegister);
        toRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewPager viewPager = getActivity().findViewById(R.id.pager);
                viewPager.setCurrentItem(1);
            }
        });
        // Inflate the layout for this fragment
        return v;
    }

    public PasienRequest createRequest(String NO_PENGENAL, String Password) {
        PasienRequest pasienRequest = new PasienRequest();
        pasienRequest.setNO_PENGENAL(NO_PENGENAL);
        pasienRequest.setPassword(Password);
        return pasienRequest;
    }

    public void pasienLogin(PasienRequest pasienRequest) {
        Call<PasienResponse> pasienResponseCall = ApiClient.getServices().pasienLogin(pasienRequest);

        pasienResponseCall.enqueue(new Callback<PasienResponse>() {
            @Override
            public void onResponse(Call<PasienResponse> call, Response<PasienResponse> response) {
                if(response.isSuccessful()) {
                    JsonObject data = response.body().getData();
                    Gson gson = new Gson();
                    Map<String, Object> map = new HashMap<String, Object>();
                    map = (Map<String, Object>) gson.fromJson(data, map.getClass());

//                    String resp = response.body().getResponse();

//                    showSnackbar(getActivity(), resp);
                    //TODO: masukkan login info ke SharedPreference + JWT
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("NO_PENGENAL", map.get("NO_PENGENAL").toString());
                    editor.putString("KD_PASIEN", map.get("KD_PASIEN").toString());
                    editor.putString("KD_CUSTOMER", map.get("KD_CUSTOMER").toString());
                    editor.putString("NAMA_LENGKAP", map.get("NAMA_LENGKAP") == null ? "":map.get("NAMA_LENGKAP").toString());
                    editor.apply();

                    if(map.get("NAMA_LENGKAP") != null) {
                        openDashboard();
                    } else {
                        openFormDataDiri();
                    }

                } else {
//                    Toast.makeText(getActivity().getBaseContext(), response.body().getResponse(), Toast.LENGTH_SHORT).show();
                    showSnackbar(getActivity(), "Login gagal!");
                }
                button.setText("Masuk");
                button.setEnabled(true);
            }
            @Override
            public void onFailure(Call<PasienResponse> call, Throwable t) {
                showSnackbar(getActivity(), t.getLocalizedMessage());
            }
        });
    }

    public void openDashboard() {
        Intent intent = new Intent(getActivity(), DashboardAct.class);
        startActivity(intent);
    }

    public void openFormDataDiri() {
        Intent intent = new Intent(getActivity(), FormDataDiri.class);
        startActivity(intent);
    }

    public void showSnackbar(Activity act, String text) {
        Snackbar.make(getView(), text, Snackbar.LENGTH_LONG).show();
    }

    public String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes(StandardCharsets.UTF_8));
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }
}