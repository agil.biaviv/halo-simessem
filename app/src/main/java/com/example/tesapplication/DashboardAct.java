package com.example.tesapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import API.ApiClient;
import API.PuskesmasResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardAct extends AppCompatActivity {
    private LinearLayout l;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_dashboard);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String nama = prefs.getString("NAMA_LENGKAP", "");
        if(nama != "") {
//            Toast.makeText(DashboardAct.this, nama, Toast.LENGTH_SHORT).show();
            Snackbar.make(findViewById(R.id.linearLayout2), nama, Snackbar.LENGTH_SHORT).show();
        }

        l = findViewById(R.id.pelayanan);
        l.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Toast.makeText(DashboardAct.this, "tes toast", Toast.LENGTH_LONG).show();
//                listPuskesmas();
//                System.out.println("Kunjungan Klik");
                openPendaftaranKunjungan();
            }
        });
    }



    public void openPendaftaranKunjungan() {
        Intent intent = new Intent(this, PendaftarKunjungan.class);
        startActivity(intent);
    }
}