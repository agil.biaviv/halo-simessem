package com.example.tesapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import API.ApiClient;
import API.DefaultResponse;
import API.GetDataResponse;
import API.KunjunganRequest;
import API.Services;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class formDaftarKunjungan extends AppCompatActivity {
    Calendar myCalendar = Calendar.getInstance();
    TextInputEditText tanggalKunjungan;
    TextInputLayout namaPasien;
    AutoCompleteTextView poli, cb;
    Button submitKunjungan;
    ArrayList<String> listPoli = new ArrayList<String>();
    ArrayList<String> kdUnit = new ArrayList<String>();
    ArrayList<String> caraBayar = new ArrayList<String>();
    ArrayList<String> kdBayar = new ArrayList<String>();
    String KD_UNIT;
    String CARA_BAYAR;

    //TODO: reset dropdown ketika provinsi kabupaten kecamatan berubah
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_form_daftar_kunjungan);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());


        int jam = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        TextView statusBuka = (TextView) findViewById(R.id.statusBuka);
        if(jam < 12 && jam > 7) {
            statusBuka.setText("Buka");
            statusBuka.setTextColor(getColor(R.color.teal_700));
        } else {
            statusBuka.setText("Tutup");
            statusBuka.setTextColor(getColor(R.color.pelayanan));
        }

        String pkm = getIntent().getStringExtra("puskesmas");
        String kdPkm = getIntent().getStringExtra("kdPkm");

        getSupportActionBar().setTitle("PUSKESMAS "+pkm);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        namaPasien = findViewById(R.id.namaPasien);
        String nama = prefs.getString("NAMA_LENGKAP", "");
        namaPasien.getEditText().setText(nama);

        // menampilkan dropdown poli
        Call<GetDataResponse> getPoli = ApiClient.getServices().getPoli(kdPkm);
        getPoli.enqueue(new Callback<GetDataResponse>() {
            @Override
            public void onResponse(Call<GetDataResponse> call, Response<GetDataResponse> response) {
                if (response.isSuccessful()) {
                    JsonArray data = response.body().getData();
                    Gson gson = new Gson();
                    List<JsonObject> puskesmas;
                    Type listData = new TypeToken<List<JsonObject>>() {
                    }.getType();
                    puskesmas = gson.fromJson(data.toString(), listData);
                    for (int i = 0; i < puskesmas.size(); i++) {
                        Map<String, Object> map = new HashMap<String, Object>();
                        map = (Map<String, Object>) new Gson().fromJson(puskesmas.get(i).toString(), map.getClass());
                        listPoli.add((String) map.get("poli"));
                        kdUnit.add((String) map.get("KD_UNIT"));
                    }
//                    String[] poli = listPoli.toArray(new String[0]);
                    poli = findViewById(R.id.poli);
                    ArrayAdapter<String> poliAdapter = new ArrayAdapter<String>(
                            formDaftarKunjungan.this,
                            R.layout.dropdown_item,
                            listPoli);
                    poli.setAdapter(poliAdapter);
                    poli.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            KD_UNIT = kdUnit.get(i);
                        }
                    });

                }
            }
            @Override
            public void onFailure(Call<GetDataResponse> call, Throwable t) {
                Log.e("API CALL", "Fetch PKM FAILED!");
                Toast.makeText(formDaftarKunjungan.this, "Gagal memuat data poli!", Toast.LENGTH_LONG).show();
            }
        });
        //======================================
        String KD_CUSTOMER = prefs.getString("KD_CUSTOMER", "");
        Call<GetDataResponse> getCaraBayar = ApiClient.getServices().getCaraBayar(KD_CUSTOMER);
        getCaraBayar.enqueue(new Callback<GetDataResponse>() {
            @Override
            public void onResponse(Call<GetDataResponse> call, Response<GetDataResponse> response) {
                if (response.isSuccessful()) {
                    JsonArray data = response.body().getData();
                    Gson gson = new Gson();
                    List<JsonObject> list_data;
                    Type listData = new TypeToken<List<JsonObject>>() {
                    }.getType();
                    list_data = gson.fromJson(data.toString(), listData);
                    for (int i = 0; i < list_data.size(); i++) {
                        Map<String, Object> map = new HashMap<String, Object>();
                        map = (Map<String, Object>) new Gson().fromJson(list_data.get(i).toString(), map.getClass());
                        caraBayar.add((String) map.get("CARA_BAYAR"));
                        kdBayar.add((String) map.get("KD_BAYAR"));
                    }
//                    String[] poli = listPoli.toArray(new String[0]);
                    cb = findViewById(R.id.cara_bayar);
                    ArrayAdapter<String> caraBayarAdapter = new ArrayAdapter<String>(
                            formDaftarKunjungan.this,
                            R.layout.dropdown_item,
                            caraBayar);
                    cb.setAdapter(caraBayarAdapter);
                    cb.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            CARA_BAYAR = caraBayar.get(i);
                        }
                    });

                }
            }

            @Override
            public void onFailure(Call<GetDataResponse> call, Throwable t) {
                Log.e("API CALL", "Fetch Cara bayar FAILED!");
                Toast.makeText(formDaftarKunjungan.this, "Gagal memuat data cara bayar!", Toast.LENGTH_LONG).show();
            }
        });


        //datepicker
//        tanggalKunjungan= (EditText) findViewById(R.id.tanggal_kunjungan);
        tanggalKunjungan = findViewById(R.id.tanggal_kunjungan);

//        CalendarConstraints.Builder calendarConstraints = new CalendarConstraints.Builder();
//        calendarConstraints.setValidator(DateValidatorPointBackward.before(MaterialDatePicker.todayInUtcMilliseconds() + 86400000));
//        calendarConstraints.setValidator(DateValidatorPointForward.now());
//
//        MaterialDatePicker.Builder datePickerBuilder = MaterialDatePicker.Builder.datePicker();
//        datePickerBuilder.setTitleText("Pilih Tanggal Kunjungan");
//        datePickerBuilder.setSelection(MaterialDatePicker.todayInUtcMilliseconds());
//        datePickerBuilder.setCalendarConstraints(calendarConstraints.build());
//
//        final MaterialDatePicker datePicker = datePickerBuilder.build();
//
//        tanggalKunjungan.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                datePicker.show(getSupportFragmentManager(), "Date Picker");
//            }
//        });
//        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
//        String date = dateFormat.format(MaterialDatePicker.todayInUtcMilliseconds());
//        tanggalKunjungan.setText(date);
//
//        datePicker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener() {
//            @Override
//            public void onPositiveButtonClick(Object selection) {
//                SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
//                String date = dateFormat.format(selection);
//                tanggalKunjungan.setText(date);
//            }
//        });
        tanggalKunjungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog date = new DatePickerDialog(formDaftarKunjungan.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH,month);
                        myCalendar.set(Calendar.DAY_OF_MONTH,day);
                        updateLabel();
                    }
                }, Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH);
                date.getDatePicker().setMinDate(System.currentTimeMillis() + (jam > 10 ? TimeUnit.DAYS.toMillis(1) : 0));
                date.getDatePicker().setMaxDate(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(1));
                date.show();
            }
        });

        submitKunjungan = findViewById(R.id.submitKunjungan);
        submitKunjungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String nik = prefs.getString("NO_PENGENAL", "");

                String TGL_MASUK = tanggalKunjungan.getText().toString();
                //kdpkm. nik, tgl_masuk, KD_UNIT, CARA_BAYAR

                requestKunjungan(createRequest(kdPkm, nik, TGL_MASUK));
//                Toast.makeText(formDaftarKunjungan.this, KD_UNIT +" "+  kdPkm, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public KunjunganRequest createRequest (String KD_PUSKESMAS, String NO_PENGENAL, String TGL_MASUK) {
        KunjunganRequest kunjunganRequest = new KunjunganRequest();
        kunjunganRequest.setKD_PUSKESMAS(KD_PUSKESMAS);
        kunjunganRequest.setTGL_MASUK(TGL_MASUK);
        kunjunganRequest.setKD_UNIT(KD_UNIT);
        kunjunganRequest.setCARA_BAYAR(CARA_BAYAR);
        kunjunganRequest.setNO_PENGENAL(NO_PENGENAL);
        return  kunjunganRequest;
    }

    public void requestKunjungan( KunjunganRequest kunjunganRequest) {
        Call<DefaultResponse> daftarKunjungan = ApiClient.getServices().daftarKunjungan(kunjunganRequest);
        daftarKunjungan.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                if(response.isSuccessful()) {
                    String msg = response.body().getResponse();
                    Snackbar.make(getCurrentFocus(), msg, Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(getCurrentFocus(), "Anda telah melakukan antrian hari ini!", Snackbar.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
//                Snackbar.make(getCurrentFocus(), "Pastikan Internet Anda Stabil!", Snackbar.LENGTH_LONG).show();
            }
        });
    }
    private void updateLabel(){
        String myFormat="yyyy-MM-dd";
        SimpleDateFormat dateFormat= new SimpleDateFormat(myFormat, Locale.US);
        tanggalKunjungan.setText(dateFormat.format(myCalendar.getTime()));
    }

    public void openNomorAntrian() {
        Intent intent = new Intent(this, NomorAntrian.class);
        startActivity(intent);
    }
}