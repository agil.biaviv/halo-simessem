package com.example.tesapplication;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.content.AsyncTaskLoader;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import API.ApiClient;
import API.PuskesmasResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PendaftarKunjungan extends AppCompatActivity {
//    String puskesmas[] = {"Sumbermalang", "Besuki", "Banyuglugur", "Bungatan", "Jatibanteng", "Widoropayung", "Suboh","Mlandingan", "Kendit", "Panarukan", "Situbondo", "Panji", "Klampokan", "Kapongan", "Mangaran", "Arjasa", "Jangkar", "Asembagus", "Banyuputih", "Wonorejo"};
    List<String> listPkm = new ArrayList<String>();
    List<String> listKdPkm = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_pendaftar_kunjungan);
        getSupportActionBar().setTitle("Pendaftaran Kunjungan");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ListView lv = findViewById(R.id.listview_puskesmas);
        ShimmerFrameLayout sv = findViewById(R.id.shimmerView);

        lv.setVisibility(View.INVISIBLE);
        sv.startShimmerAnimation();
        Call<PuskesmasResponse> getPuskesmas = ApiClient.getServices().getPuskesmas();
        getPuskesmas.enqueue(new Callback<PuskesmasResponse>() {
            @Override
            public void onResponse(Call<PuskesmasResponse> call, Response<PuskesmasResponse> response) {
                if(response.isSuccessful()){
                    JsonArray data = response.body().getData();
                    Gson gson = new Gson();
                    List<JsonObject> puskesmas;
                    Type listData = new TypeToken<List<JsonObject>>() {}.getType();
                    puskesmas = gson.fromJson(data.toString(), listData);
                    for(int i = 0; i < puskesmas.size(); i++){
                        Map<String, Object> map = new HashMap<String, Object>();
                        map = (Map<String, Object>) new Gson().fromJson(puskesmas.get(i).toString(), map.getClass());
                        listPkm.add((String) map.get("PUSKESMAS"));
                        listKdPkm.add((String) map.get("KD_PUSKESMAS"));
                    }
                    String[] pkm = listPkm.toArray(new String[0]);
                    String[] kdPkm = listKdPkm.toArray(new String[0]);
                    sv.stopShimmerAnimation();
                    lv.setVisibility(View.VISIBLE);
                    sv.setVisibility(View.INVISIBLE);
                    ListPuskesmasAdapter listPuskesmasAdapter = new ListPuskesmasAdapter(getApplicationContext(), pkm);
                    lv.setAdapter(listPuskesmasAdapter);

                    lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            openFormKunjungan(pkm[i], kdPkm[i]);
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<PuskesmasResponse> call, Throwable t) {
                Log.e("API CALL", "Fetch PKM FAILED!");
                Toast.makeText(PendaftarKunjungan.this, "Gagal memuat data puskesmas!", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void openFormKunjungan(String namaPkm, String kdPkm) {
        Intent intent = new Intent(this, formDaftarKunjungan.class);
        intent.putExtra("puskesmas", namaPkm);
        intent.putExtra("kdPkm", kdPkm);
        startActivity(intent);
    }

}