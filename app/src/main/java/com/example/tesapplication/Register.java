package com.example.tesapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.text.TextUtils;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputEditText;

import java.nio.charset.StandardCharsets;
import java.util.zip.Inflater;

import API.ApiClient;
import API.DefaultResponse;
import API.RegisterRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register extends Fragment implements View.OnClickListener{

    EditText password, confPassword, nik, nohp;
    Button registerBtn;
    View v;
    TabLayout tab;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_register, container, false);
        nik = v.findViewById(R.id.nikRegister);
        nohp = v.findViewById(R.id.noHpRegister);
        password = v.findViewById(R.id.passRegister);
        confPassword = v.findViewById(R.id.confPass);
        registerBtn = v.findViewById(R.id.registerBtn);

        registerBtn.setOnClickListener(this);

        // Inflate the layout for this fragment
        return v;
    }

    public RegisterRequest createRequest(String NO_PENGENAL, String HP, String Password) {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setNO_PENGENAL(NO_PENGENAL);
        registerRequest.setHP(HP);
        registerRequest.setPassword(MD5(Password));
        return registerRequest;
    }

    public void pasienRegsiter(DefaultResponse defaultResponse) {
        Call<DefaultResponse> defaultResponseCall = ApiClient.getServices().pasienRegister();
        defaultResponseCall.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                if(response.isSuccessful()) {
                    String msg = response.body().getResponse();
                    Snackbar.make(v.findViewById(R.id.linearLayout2), msg, Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        if(TextUtils.isEmpty(nik.getText())) {
            nik.setError("Nomor Kartu Penduduk harus diisi!");
            return;
        }
        if(TextUtils.isEmpty(nohp.getText())) {
            nohp.setError("No. Handphone harus diisi!");
            return;
        }
        if(TextUtils.isEmpty(password.getText())) {
            password.setError("Kata Sandi Harus diisi!");
            return;
        }
        if(TextUtils.isEmpty(confPassword.getText())) {
            confPassword.setError("Ketikkan ulang kata sandi!");
            return;
        }
        String pass = password.getText().toString();
        String pass2 = confPassword.getText().toString();
//        Snackbar.make(view, pass, Snackbar.LENGTH_LONG).show();

        if(pass.equals(pass2)) {
            Snackbar.make(view, "Yok gas registrasi!", Snackbar.LENGTH_LONG).show();
        } else {
            Snackbar.make(view, "Kata Sandi berbeda! ", Snackbar.LENGTH_LONG).show();
        }
    }

    public String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes(StandardCharsets.UTF_8));
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }


}