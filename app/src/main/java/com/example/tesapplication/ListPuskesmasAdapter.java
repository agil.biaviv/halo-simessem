package com.example.tesapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ListPuskesmasAdapter extends BaseAdapter {
    Context context;
    String[] namaPkm;
    LayoutInflater inflater;

    ListPuskesmasAdapter(Context c, String[] nama_puskesmas) {
        this.context = c;
        this.namaPkm = nama_puskesmas;
        inflater = LayoutInflater.from(c);
    }
    @Override
    public int getCount() {
        return namaPkm.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.list_puskesmas, null);
        TextView nama = (TextView) view.findViewById(R.id.nama_puskesmas);
        nama.setText("PUSKESMAS " + namaPkm[i]);
        return view;
    }
}
