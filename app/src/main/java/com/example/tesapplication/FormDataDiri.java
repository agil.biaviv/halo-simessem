package com.example.tesapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.DateValidatorPointBackward;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import API.GetDataResponse;
import API.ApiClient;
import API.DefaultResponse;
import API.UpdateDataDiriRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FormDataDiri extends AppCompatActivity {
    TextInputEditText tanggalLahir;
    ArrayList<String> provinsi = new ArrayList<String>();
    ArrayList<String> kabupaten = new ArrayList<String>();
    ArrayList<String> kecamatan = new ArrayList<String>();
    ArrayList<String> kelurahan = new ArrayList<String>();
    ArrayList<String> kdProvinsi = new ArrayList<String>();
    ArrayList<String> kdKabupaten = new ArrayList<String>();
    ArrayList<String> kdKecamatan =  new ArrayList<String>();
    ArrayList<String> kdKelurahan =  new ArrayList<String>();
    TextInputLayout layoutNama, layoutTempatLahir, layoutProvinsi, layoutKabupaten, layoutKecamatan, layoutKelurahan, layoutAlamat;
    TextInputEditText alamat;
    AutoCompleteTextView txProvinsi, txKabupaten, txKecamatan, txKelurahan;
    RadioGroup JENIS_KELAMIN;
    Button submit;

    String KD_PROVINSI, KD_KABUPATEN, KD_KECAMATAN, KD_KELURAHAN, NO_PENGENAL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_form_data_diri);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        NO_PENGENAL = prefs.getString("NO_PENGENAL", "");

        layoutNama = findViewById(R.id.formDataDiri_nama_parent);
        layoutTempatLahir = findViewById(R.id.formDataDiri_tempat_lahir_parent);
        tanggalLahir = findViewById(R.id.formDataDiri_tanggal_lahir);
        txKabupaten = (AutoCompleteTextView) findViewById(R.id.formDataDiri_kabupaten);
        txKecamatan = (AutoCompleteTextView) findViewById(R.id.formDataDiri_kecamatan);
        txKelurahan = (AutoCompleteTextView) findViewById(R.id.formDataDiri_kelurahan);
        layoutProvinsi = findViewById(R.id.formDataDiri_provinsi_layout);
        layoutKabupaten = findViewById(R.id.formDataDiri_kabupaten_parent);
        layoutKecamatan = findViewById(R.id.formDataDiri_kecamatan_parent);
        layoutKelurahan = findViewById(R.id.formDataDiri_kelurahan_parent);
        layoutAlamat = findViewById(R.id.formDataDiri_alamat_parent);
        alamat = findViewById(R.id.formDataDiri_alamat);




        CalendarConstraints.Builder calendarConstraints = new CalendarConstraints.Builder();
        calendarConstraints.setValidator(DateValidatorPointBackward.now());

        MaterialDatePicker.Builder datePickerBuilder = MaterialDatePicker.Builder.datePicker();
        datePickerBuilder.setTitleText("Pilih Tanggal Lahir");
        datePickerBuilder.setSelection(MaterialDatePicker.todayInUtcMilliseconds());
        datePickerBuilder.setCalendarConstraints(calendarConstraints.build());

        final MaterialDatePicker datePicker = datePickerBuilder.build();
        tanggalLahir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePicker.show(getSupportFragmentManager(), "Date Picker");
            }
        });

        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
        String date = dateFormat.format(MaterialDatePicker.todayInUtcMilliseconds());
        tanggalLahir.setText(date);

        datePicker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener() {
            @Override
            public void onPositiveButtonClick(Object selection) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
                String date = dateFormat.format(selection);
                tanggalLahir.setText(date);
            }
        });

        Call<GetDataResponse> getProvinsi = ApiClient.getServices().getProvinsi();
        getProvinsi.enqueue(new Callback<GetDataResponse>() {
            @Override
            public void onResponse(Call<GetDataResponse> call, Response<GetDataResponse> response) {
                if (response.isSuccessful()) {
                    JsonArray data = response.body().getData();
                    Gson gson = new Gson();
                    List<JsonObject> objProvinsi;
                    Type listProvinsi = new TypeToken<List<JsonObject>>(){}.getType();
                    objProvinsi = gson.fromJson(data.toString(), listProvinsi);
                    for (int i = 0; i < objProvinsi.size();i++) {
                        Map<String, Object> map = new HashMap<String, Object>();
                        map = (Map<String, Object>) new Gson().fromJson(objProvinsi.get(i).toString(), map.getClass());
                        provinsi.add((String) map.get("PROVINSI"));
                        kdProvinsi.add((String) map.get("KD_PROVINSI"));
                    }
                    txProvinsi = (AutoCompleteTextView) findViewById(R.id.formDataDiri_provinsi);
                    ArrayAdapter<String> provinsiAdapter = new ArrayAdapter<String>(
                            FormDataDiri.this,
                            R.layout.dropdown_item,
                            provinsi
                    );
                    txProvinsi.setAdapter(provinsiAdapter);
                    txProvinsi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                            resetForm();
                            String kd = kdProvinsi.get(position);
                            getKabupaten(kd);
                            KD_PROVINSI = kd;
                            layoutKecamatan.setVisibility(View.GONE);
                            layoutKelurahan.setVisibility(View.GONE);
                            layoutAlamat.setVisibility(View.GONE);
                            if(layoutKabupaten.getVisibility() != View.VISIBLE) {
                                layoutKabupaten.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
            }
            @Override
            public void onFailure(Call<GetDataResponse> call, Throwable t) {

            }
        });
        JENIS_KELAMIN = findViewById(R.id.formdatadiri_jenis_kelamin);
        submit = findViewById(R.id.formDataDiri_selesai);

        alamat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() == 0) {
                    resetForm();
                } else {
                    submit.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String NAMA_LENGKAP = layoutNama.getEditText().getText().toString();
                if(NAMA_LENGKAP.length() == 0) {
                    layoutNama.getEditText().setError("Nama harus diisi!");
                    layoutNama.getEditText().requestFocus();
                    return;
                }
                String TEMPAT_LAHIR = layoutTempatLahir.getEditText().getText().toString();
                if(TEMPAT_LAHIR.length() == 0) {
                    layoutTempatLahir.getEditText().setError("Tempat lahir harus diisi!");
                    layoutTempatLahir.getEditText().requestFocus();
                    return;
                }
                String TGL_LAHIR = tanggalLahir.getText().toString();
                String KD_JENIS_KELAMIN = "";
                int selectedJenisKelamin = JENIS_KELAMIN.getCheckedRadioButtonId();
                RadioButton jk = findViewById(selectedJenisKelamin);

                if(jk.getText().toString().equals("Perempuan"))
                    KD_JENIS_KELAMIN = "P";
                else
                    KD_JENIS_KELAMIN = "L";
                String ALAMAT = layoutAlamat.getEditText().getText().toString();

                String[] data = {
                        NO_PENGENAL,
                        NAMA_LENGKAP,
                        KD_JENIS_KELAMIN,
                        TEMPAT_LAHIR,
                        TGL_LAHIR,
                        KD_PROVINSI,
                        KD_KABUPATEN,
                        KD_KECAMATAN,
                        KD_KELURAHAN,
                        ALAMAT,
                        "TU" //cara_bayar
                };

                setUpdateDataDiri(createRequest(data));

//                Snackbar.make(findViewById(R.id.formDataDiri_layout), Arrays.toString(data), Snackbar.LENGTH_SHORT).show();
            }
        });

    }

    public void getKabupaten(String kdProvinsi) {
        Call<GetDataResponse> getKabupaten = ApiClient.getServices().getKabupaten(kdProvinsi);
        getKabupaten.enqueue(new Callback<GetDataResponse>() {
            @Override
            public void onResponse(Call<GetDataResponse> call, Response<GetDataResponse> response) {
                JsonArray data = response.body().getData();
                Gson gson = new Gson();
                List<JsonObject> objKabupaten;
                Type listProvinsi = new TypeToken<List<JsonObject>>(){}.getType();
                objKabupaten = gson.fromJson(data.toString(), listProvinsi);
                kabupaten.clear();
                kdKabupaten.clear();
                kecamatan.clear();
                kdKecamatan.clear();
                kelurahan.clear();
                kdKelurahan.clear();
                for (int i = 0; i < objKabupaten.size(); i++) {
                    Map<String, Object> map = new HashMap<String, Object>();
                    map = (Map<String, Object>) new Gson().fromJson(objKabupaten.get(i).toString(), map.getClass());
                    kabupaten.add((String) map.get("KABUPATEN"));
                    kdKabupaten.add((String) map.get("KD_KABUPATEN"));
                }
                ArrayAdapter<String> kabupatenAdapter = new ArrayAdapter<>(
                        FormDataDiri.this,
                        R.layout.dropdown_item,
                        kabupaten
                );
                txKabupaten.setAdapter(kabupatenAdapter);
                txKabupaten.setText("", false);
                txKabupaten.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        resetForm();
                        String kd = kdKabupaten.get(i);
                        getKecamatan(kd);
                        KD_KABUPATEN = kd;
                        layoutKelurahan.setVisibility(View.GONE);
                        layoutAlamat.setVisibility(View.GONE);
                        if(layoutKecamatan.getVisibility() != View.VISIBLE) {
                            layoutKecamatan.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }

            @Override
            public void onFailure(Call<GetDataResponse> call, Throwable t) {
            }
        });
    }

    public void getKecamatan(String kdKabupaten) {
        Call<GetDataResponse> getKecamatan = ApiClient.getServices().getKecamatan(kdKabupaten);
        getKecamatan.enqueue(new Callback<GetDataResponse>() {
            @Override
            public void onResponse(Call<GetDataResponse> call, Response<GetDataResponse> response) {
                JsonArray data = response.body().getData();
                Gson gson = new Gson();
                List<JsonObject> objKecamatan;
                Type listKecamatan = new TypeToken<List<JsonObject>>(){}.getType();
                objKecamatan = gson.fromJson(data.toString(), listKecamatan);
                kecamatan.clear();
                kdKecamatan.clear();
                kelurahan.clear();
                kdKelurahan.clear();
                for (int i = 0; i < objKecamatan.size(); i++) {
                    Map<String, Object> map = new HashMap<String, Object>();
                    map = (Map<String, Object>) new Gson().fromJson(objKecamatan.get(i).toString(), map.getClass());
                    kecamatan.add((String) map.get("KECAMATAN"));
                    kdKecamatan.add((String) map.get("KD_KECAMATAN"));
                }
                ArrayAdapter<String> kecamatanAdapter = new ArrayAdapter<>(
                        FormDataDiri.this,
                        R.layout.dropdown_item,
                        kecamatan
                );
                txKecamatan.setAdapter(kecamatanAdapter);
                txKecamatan.setText("", false);
                txKecamatan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        resetForm();
                        String kd = kdKecamatan.get(i);
                        getKelurahan(kd);
                        KD_KECAMATAN = kd;
                        layoutAlamat.setVisibility(View.GONE);
                        if(layoutKelurahan.getVisibility() != View.VISIBLE) {
                            layoutKelurahan.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
            @Override
            public void onFailure(Call<GetDataResponse> call, Throwable t) {
            }
        });
    }

    public void getKelurahan(String kdKecamatan) {
        Call<GetDataResponse> getKelurahan = ApiClient.getServices().getKelurahan(kdKecamatan);
        getKelurahan.enqueue(new Callback<GetDataResponse>() {
            @Override
            public void onResponse(Call<GetDataResponse> call, Response<GetDataResponse> response) {
                JsonArray data = response.body().getData();
                Gson gson = new Gson();
                List<JsonObject> objKelurahan;
                Type listKelurahan = new TypeToken<List<JsonObject>>(){}.getType();
                objKelurahan = gson.fromJson(data.toString(), listKelurahan);
                kelurahan.clear();
                kdKelurahan.clear();
                for (int i = 0; i < objKelurahan.size(); i++) {
                    Map<String, Object> map = new HashMap<String, Object>();
                    map = (Map<String, Object>) new Gson().fromJson(objKelurahan.get(i).toString(), map.getClass());
                    kelurahan.add((String) map.get("KELURAHAN"));
                    kdKelurahan.add((String) map.get("KD_KELURAHAN"));
                }
                ArrayAdapter<String> kelurahanAdapter = new ArrayAdapter<>(
                        FormDataDiri.this,
                        R.layout.dropdown_item,
                        kelurahan
                );
                txKelurahan.setAdapter(kelurahanAdapter);
                txKelurahan.setText("", false);
                txKelurahan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        resetForm();
                        if(layoutAlamat.getVisibility() != View.VISIBLE) {
                            layoutAlamat.setVisibility(View.VISIBLE);
                        }
                        KD_KELURAHAN = kdKelurahan.get(i);
                    }
                });
            }

            @Override
            public void onFailure(Call<GetDataResponse> call, Throwable t) {
            }
        });
    }

    public void resetForm () {
       alamat.getText().clear();
        if(submit.isEnabled()) {
            submit.setEnabled(false);
        }
    }

    public UpdateDataDiriRequest createRequest(String[] data) {
        UpdateDataDiriRequest updateDataDiriRequest = new UpdateDataDiriRequest();
        updateDataDiriRequest.setNO_PENGENAL(data[0]);
        updateDataDiriRequest.setNAMA_LENGKAP(data[1]);
        updateDataDiriRequest.setKD_JENIS_KELAMIN(data[2]);
        updateDataDiriRequest.setTEMPAT_LAHIR(data[3]);
        updateDataDiriRequest.setTGL_LAHIR(data[4]);
        updateDataDiriRequest.setKD_PROVINSI(data[5]);
        updateDataDiriRequest.setKD_KABKOTA(data[6]);
        updateDataDiriRequest.setKD_KECAMATAN(data[7]);
        updateDataDiriRequest.setKD_KELURAHAN(data[8]);
        updateDataDiriRequest.setALAMAT(data[9]);
        updateDataDiriRequest.setCARA_BAYAR(data[10]);
        return updateDataDiriRequest;
    }

    public void setUpdateDataDiri(UpdateDataDiriRequest updateDataDiriRequest) {
        Call<DefaultResponse> updateDataDiri = ApiClient.getServices().setUpdateDataDiri(updateDataDiriRequest);
        updateDataDiri.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                if(response.isSuccessful()) {
                    String msg = response.body().getResponse();
                    Boolean status = response.body().getStatus();
                    Snackbar.make(findViewById(R.id.formDataDiri_layout), msg, Snackbar.LENGTH_SHORT).show();
                    openDashboard();
                } else {
                    Snackbar.make(findViewById(R.id.formDataDiri_layout), "Update data gagal!", Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {

            }
        });


    }

    public void openDashboard() {
        Intent intent = new Intent(this, DashboardAct.class);
        startActivity(intent);
    }


}