package com.example.tesapplication;

import android.os.Parcel;

import com.google.android.material.datepicker.CalendarConstraints;

import java.util.Calendar;
import java.util.TimeZone;
import com.google.android.material.datepicker.CalendarConstraints.DateValidator;

public class DateValidatorKunjungan implements DateValidator {
    private Calendar utc = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

    public final Creator<DateValidatorKunjungan> CREATOR = new Creator<DateValidatorKunjungan>(){
        @Override
        public DateValidatorKunjungan createFromParcel(Parcel source) {
            return new DateValidatorKunjungan();
        }

        @Override
        public DateValidatorKunjungan[] newArray(int size) {
            return new DateValidatorKunjungan[size];
        }
    };

    @Override
    public boolean isValid(long date) {
        utc.setTimeInMillis(date);
        int tglKunjungan = utc.get(Calendar.DAY_OF_MONTH);

        return tglKunjungan != Calendar.SATURDAY && tglKunjungan != Calendar.SUNDAY;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }
}
