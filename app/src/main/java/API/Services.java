package API;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface Services {

    @POST("login/")
    Call<PasienResponse> pasienLogin(@Body PasienRequest pasienRequest);

    @GET ("puskesmas/")
    Call<PuskesmasResponse> getPuskesmas();

    @GET("puskesmas/poli/{puskesmas}")
    Call<GetDataResponse> getPoli(@Path("puskesmas") String kdPkm);

    @POST("pasien/")
    Call<DefaultResponse> pasienRegister();

    @PATCH("pasien/")
    Call<DefaultResponse> setUpdateDataDiri(@Body UpdateDataDiriRequest updateDataDiriRequest);

    @GET("pembayaran/{kodeCustomer}")
    Call<GetDataResponse> getCaraBayar(@Path("kodeCustomer") String kdCustomer);

    @GET("provinsi/")
    Call<GetDataResponse> getProvinsi();

    @GET("kabupaten/{provinsi}")
    Call<GetDataResponse> getKabupaten(@Path("provinsi") String kdProvinsi);

    @GET("kecamatan/{kabupaten}")
    Call<GetDataResponse> getKecamatan(@Path("kabupaten") String kdKabupaten);

    @GET("kelurahan/{kecamatan}")
    Call<GetDataResponse> getKelurahan(@Path("kecamatan") String KdKecamatan);

    @POST("kunjungan/")
    Call<DefaultResponse> daftarKunjungan(@Body KunjunganRequest kunjunganRequest);
}
