package API;

public class UpdateDataDiriRequest {
    private String NO_PENGENAL;
    private String NAMA_LENGKAP;
    private String KD_JENIS_KELAMIN;
    private String TEMPAT_LAHIR;
    private String TGL_LAHIR;
    private String KD_PROVINSI;
    private String KD_KABKOTA;
    private String KD_KECAMATAN;
    private String KD_KELURAHAN;
    private String CARA_BAYAR;
    private String ALAMAT;

    public String getALAMAT() {
        return ALAMAT;
    }

    public void setALAMAT(String ALAMAT) {
        this.ALAMAT = ALAMAT;
    }

    private String NO_ASURANSI;

    public String getNO_PENGENAL() {
        return NO_PENGENAL;
    }

    public void setNO_PENGENAL(String NO_PENGENAL) {
        this.NO_PENGENAL = NO_PENGENAL;
    }

    public String getNAMA_LENGKAP() {
        return NAMA_LENGKAP;
    }

    public void setNAMA_LENGKAP(String NAMA_LENGKAP) {
        this.NAMA_LENGKAP = NAMA_LENGKAP;
    }

    public String getKD_JENIS_KELAMIN() {
        return KD_JENIS_KELAMIN;
    }

    public void setKD_JENIS_KELAMIN(String KD_JENIS_KELAMIN) {
        this.KD_JENIS_KELAMIN = KD_JENIS_KELAMIN;
    }

    public String getTEMPAT_LAHIR() {
        return TEMPAT_LAHIR;
    }

    public void setTEMPAT_LAHIR(String TEMPAT_LAHIR) {
        this.TEMPAT_LAHIR = TEMPAT_LAHIR;
    }

    public String getTGL_LAHIR() {
        return TGL_LAHIR;
    }

    public void setTGL_LAHIR(String TGL_LAHIR) {
        this.TGL_LAHIR = TGL_LAHIR;
    }

    public String getKD_PROVINSI() {
        return KD_PROVINSI;
    }

    public void setKD_PROVINSI(String KD_PROVINSI) {
        this.KD_PROVINSI = KD_PROVINSI;
    }

    public String getKD_KABKOTA() {
        return KD_KABKOTA;
    }

    public void setKD_KABKOTA(String KD_KABKOTA) {
        this.KD_KABKOTA = KD_KABKOTA;
    }

    public String getKD_KECAMATAN() {
        return KD_KECAMATAN;
    }

    public void setKD_KECAMATAN(String KD_KECAMATAN) {
        this.KD_KECAMATAN = KD_KECAMATAN;
    }

    public String getKD_KELURAHAN() {
        return KD_KELURAHAN;
    }

    public void setKD_KELURAHAN(String KD_KELURAHAN) {
        this.KD_KELURAHAN = KD_KELURAHAN;
    }

    public String getCARA_BAYAR() {
        return CARA_BAYAR;
    }

    public void setCARA_BAYAR(String CARA_BAYAR) {
        this.CARA_BAYAR = CARA_BAYAR;
    }

    public String getNO_ASURANSI() {
        return NO_ASURANSI;
    }

    public void setNO_ASURANSI(String NO_ASURANSI) {
        this.NO_ASURANSI = NO_ASURANSI;
    }
}
