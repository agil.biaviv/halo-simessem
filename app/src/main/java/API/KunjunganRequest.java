package API;

public class KunjunganRequest {
    String KD_PUSKESMAS;
    String TGL_MASUK;
    String KD_UNIT;
    String CARA_BAYAR;
    String NO_PENGENAL;

    public String getKD_PUSKESMAS() {
        return KD_PUSKESMAS;
    }

    public void setKD_PUSKESMAS(String KD_PUSKESMAS) {
        this.KD_PUSKESMAS = KD_PUSKESMAS;
    }

    public String getTGL_MASUK() {
        return TGL_MASUK;
    }

    public void setTGL_MASUK(String TGL_MASUK) {
        this.TGL_MASUK = TGL_MASUK;
    }

    public String getKD_UNIT() {
        return KD_UNIT;
    }

    public void setKD_UNIT(String KD_UNIT) {
        this.KD_UNIT = KD_UNIT;
    }

    public String getCARA_BAYAR() {
        return CARA_BAYAR;
    }

    public void setCARA_BAYAR(String CARA_BAYAR) {
        this.CARA_BAYAR = CARA_BAYAR;
    }

    public String getNO_PENGENAL() {
        return NO_PENGENAL;
    }

    public void setNO_PENGENAL(String NO_PENGENAL) {
        this.NO_PENGENAL = NO_PENGENAL;
    }
}
