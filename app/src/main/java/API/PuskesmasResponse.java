
package API;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class PuskesmasResponse {
private JsonArray data;

    public JsonArray getData() {
        return data;
    }

    public void setData(JsonArray data) {
        this.data = data;
    }
}
