package API;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;

public class PasienResponse {
    private String Status;
    private String Response;
    private JsonObject data;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        this.Status = status;
    }

    public String getResponse() {
        return Response;
    }

    public void setResponse(String response) {
        Response = response;
    }

    public JsonObject getData() {
        return data;
    }

    public void setData(JsonObject d) {
        data = d;
    }
}
